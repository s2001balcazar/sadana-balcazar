
document.addEventListener("DOMContentLoaded", function() {
document.getElementById("formulario").addEventListener('submit', validarFormulario); 
});

function validarFormulario(evento) {
evento.preventDefault();
var codigo = document.getElementById('codigo').value;
var alfanumerico = /^[0-9a-zA-z]{1}+&/
if(codigo.length != 5) {
  alert('El codigo debe tener 5 caracteres');
  return;
}
else{
  if(!codigo.match(alfanumerico)){
      return alert("Introducir al menos una letra y un numero");
  }
}
var titulo = document.getElementById('titulo').value;
if(titulo.length != 100) {
  alert('El titulo debe tener 100 caracteres');
  return;
}
else{
  if(!titulo.match(alfanumerico)){
      return alert("Introducir al menos una letra y un numero");
  }
}

var autor = document.getElementById('autor').value;
if(autor.length != 60) {
  alert('El codigo debe tener 60 caracteres');
  return;
}
else{
  if(!autor.match(alfanumerico)){
      return alert("Introducir al menos una letra y un numero");
  }
}

var editorial = document.getElementById('editorial').value;
if(editorial.length != 30) {
  alert('El codigo debe tener 5 caracteres');
  return;
}
else{
  if(!editorial.match(alfanumerico)){
      return alert("Introducir al menos una letra y un numero");
  }
}

var fecha_publicacion = document.getElementById('fecha_publicacion').value;

var tipofecha = /^\d{1,2}\/\d{1,2}\/\d{2,4}$/;


 if ((fecha_publicacion.match(tipofecha)) && (fecha_publicacion!='')) {
          return true;
  } else {
      alert("Debe ser fecha")
  }
var fecha_ingreso = document.getElementById('fecha_ingreso').value;

if ((fecha_ingreso.match(tipofecha)) && (fecha_ingreso!='')) {
          return true;
  } else {
      alert("Debe ser fecha")
  }


if(fecha_ingreso < fecha_publicacion) {
  alert('Fecha de ingreso debe ser mayor a fecha de publicacion');
  return;
}

this.submit();
}
